#include "block.h"
#include "scene.h"

//�����������
Block::Block(QGraphicsItem *parent, QMenu *menu)
	: QGraphicsItem(parent)
{
	//�������� ������������ ����
	this->contextMenu = menu;
	//��������� ������ ����������� � ���������
	this->setFlag(QGraphicsItem::ItemIsMovable);
	this->setFlag(QGraphicsItem::ItemIsSelectable);
}

Block::~Block()
{

}

//��������� ������������ ����
void Block::contextMenuEvent(QGraphicsSceneContextMenuEvent*mouseEvent)
{
	//���� ������� - work
	if(this->type() == ElementType::work)
	{
		//�������� ������� � ��������� ����������� ����
		this->setSelected(true);
		this->contextMenu->exec(mouseEvent->screenPos());
	}
	else
		return;
}

//���������� ������� � �����
bool Block::addArrow(Arrow* arrow)
{
	//�������� �� ������� ����������� ������� � ����������
	for(int i=0; i<arrows.size(); i++)
		//���� ������� ��� ����������, �� ����� ��� ����������
		if(arrows[i] == arrow)
			return false;
	//�����, ��������� ������� � ���������
	arrows << arrow;
	return true;
}

//��������� ������� �����
QVariant Block::itemChange(GraphicsItemChange change, const QVariant &value)
{
	//�������� �� ��������� ������� �����
	if(change == ItemPositionChange)
	{
		//������ ������� ��������� �������
		for(int i=0; i<arrows.size(); i++)
			arrows[i]->update();
	}
	return QGraphicsItem::itemChange(change,value);
}

//�������� �������
void Block::removeArrow(Arrow* arrow)
{
	//����� � ���������� ��������� �������
	int index = this->arrows.indexOf(arrow);
	//�������� �������
	if(index >= 0)
		this->arrows.remove(index);
}

//�������� ���� ��������� �������
void Block::removeArrows()
{
	//�������� ���������� �� �������
	if(arrows.size())
	{
		//���������� ������ ������� � ����������
		foreach (Arrow *arrow, arrows)
		{
			//������� ��� ��� ��������� � �������� ����� � ������� ������� � ���
			Block *firstItem = qgraphicsitem_cast<Block*>(arrow->firstItem());
			Block *secondItem = qgraphicsitem_cast<Block*>(arrow->secondItem());
			firstItem->removeArrow(arrow);
			secondItem->removeArrow(arrow);
			this->scene()->removeItem(arrow);
		}
	}
	else
		return;
}