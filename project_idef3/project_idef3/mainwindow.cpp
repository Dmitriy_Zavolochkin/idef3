#include "mainwindow.h"
#include "QFileDialog"

MainWindow::MainWindow(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);

	ui.actionNew->setShortcut(Qt::Key::Key_N);
	ui.actionOpen->setShortcut(Qt::Key::Key_O);
	ui.actionSave_as->setShortcut(Qt::Key::Key_S);
	ui.actionExit->setShortcut(Qt::Key::Key_Q);
	ui.addWorkBtn->setShortcut(Qt::Key::Key_W);
	ui.addCross1Btn->setShortcut(Qt::Key::Key_1);
	ui.addCross2Btn->setShortcut(Qt::Key::Key_2);
	ui.addCross3Btn->setShortcut(Qt::Key::Key_3);
	ui.addCross4Btn->setShortcut(Qt::Key::Key_4);
	ui.addCross5Btn->setShortcut(Qt::Key::Key_5);
	ui.scaleUpBtn->setShortcut(Qt::Key::Key_Plus);
	ui.scaleDownBtn->setShortcut(Qt::Key::Key_Minus);
	ui.addArrow1Btn->setShortcut(Qt::Key::Key_1);
	ui.addArrow2Btn->setShortcut(Qt::Key::Key_2);
	ui.addArrow3Btn->setShortcut(Qt::Key::Key_3);

	ui.filename->setVisible(false);

	createWorkContextMenu();
	createArrowContextMenu();

	this->view = ui.scene;
	this->scene = new Scene(this, WorkContextMenu, this->ArrowContextMenu);
	this->view->setScene(this->scene);
	this->view->setSceneRect(QRectF(0, 0, 3000, 1800));
	this->view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
	this->view->setDragMode(QGraphicsView::RubberBandDrag);

	textWork = new TextBlock();
	textArrow = new TextArrow();

	connect(textWork, SIGNAL(oktextblock(QString,QString)), this, SLOT(addTextWork(QString,QString)));
	connect(textArrow, SIGNAL(oktextarrow(QString)), this, SLOT(addTextArrow(QString)));
	connect(ui.addWorkBtn, SIGNAL(clicked()), this, SLOT(addWork()));
	connect(ui.addCross1Btn, SIGNAL(clicked()), this, SLOT(addCross1()));
	connect(ui.addCross2Btn, SIGNAL(clicked()), this, SLOT(addCross2()));
	connect(ui.addCross3Btn, SIGNAL(clicked()), this, SLOT(addCross3()));
	connect(ui.addCross4Btn, SIGNAL(clicked()), this, SLOT(addCross4()));
	connect(ui.addCross5Btn, SIGNAL(clicked()), this, SLOT(addCross5()));
	connect(ui.actionNew, SIGNAL(triggered()), this, SLOT(actionNew()));
	connect(ui.actionSave, SIGNAL(triggered()), this, SLOT(actionSave()));
	connect(ui.actionSave_as, SIGNAL(triggered()), this, SLOT(actionSave_as()));
	connect(ui.actionOpen, SIGNAL(triggered()), this, SLOT(actionOpen()));
	connect(ui.actionExport_to_GIF, SIGNAL(triggered()), this, SLOT(actionExport_to_PNG()));
	connect(ui.actionExport_to_PNG_with, SIGNAL(triggered()), this, SLOT(actionExport_to_PNG_with()));
	connect(ui.actionExit, SIGNAL(triggered()), this, SLOT(actionExit()));
	connect(ui.addArrow1Btn, SIGNAL(clicked()), this, SLOT(addArrow1()));
	connect(ui.addArrow2Btn, SIGNAL(clicked()), this, SLOT(addArrow2()));
	connect(ui.addArrow3Btn, SIGNAL(clicked()), this, SLOT(addArrow3()));
	connect(ui.scaleUpBtn, SIGNAL(clicked()), this, SLOT(scaleUp()));
	connect(ui.scaleDownBtn, SIGNAL(clicked()), this, SLOT(scaleDown()));

	readSettings();
}

MainWindow::~MainWindow()
{
}

void MainWindow::writeSettings()
{
	QSettings settings("IVT-462", "idef3_editor");
	
	settings.beginGroup("MainWindow");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
}

void MainWindow::readSettings()
{
	QSettings settings("IVT-462", "idef3_editor");

	settings.beginGroup("MainWindow");
     resize(settings.value("size", QSize(1000, 513)).toSize());
     move(settings.value("pos", QPoint(200, 200)).toPoint());
     settings.endGroup();
}

void MainWindow::actionNew()
{
	ui.statusBar->showMessage(tr("Scene was cleared"), 1500);
	this->scene->clear();
}

void MainWindow::actionSave_as()
{
	QString FileName;
	FileName = QFileDialog::getSaveFileName (this, "Save file as...", "NewDiagram.i3d", "idef3 (*.i3d)");
	QFile file(FileName);
	if(file.open(QIODevice::WriteOnly))		        //��������� ���� ������ ��� ������
	{
		QXmlStreamWriter xmlWriter(&file);
		xmlWriter.setAutoFormatting(true);
		xmlWriter.writeStartDocument();
		xmlWriter.writeStartElement("SceneData");
		xmlWriter.writeAttribute("version", "v1.0");
		xmlWriter.writeStartElement("GraphicsItemList");
		foreach( QGraphicsItem* item, scene->items())  
		{  
			int ct = item->type();

			// ��������� ������
			//------------------------------------
			if( item->type() == ElementType::work )  
			{  
				Work* myItem = (Work*)item;  
				xmlWriter.writeStartElement("Work");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeAttribute("number", myItem->getNumber());
				xmlWriter.writeAttribute("textWork", myItem->getText());
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			} 

			// ��������� ����������� �����������
			//----------------------------------
			// Xor
			if( item->type() == crossType::Xor )  
			{  
				Cross* myItem = (Cross*)item;  
				xmlWriter.writeStartElement("Xor");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}
			// SynchroOr
			if( item->type() == crossType::SinchroOr )  
			{  
				Cross* myItem = (Cross*)item;  
				xmlWriter.writeStartElement("SynchroOr");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}
			// SynchroAnd
			if( item->type() == crossType::SinchroAnd )  
			{  
				Cross* myItem = (Cross*)item;  
				xmlWriter.writeStartElement("SynchroAnd");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}
			// AsynchroOr
			if( item->type() == crossType::AsinchroOr )  
			{  
				Cross* myItem = (Cross*)item;  
				xmlWriter.writeStartElement("AsynchroOr");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}
			// AsynchroAnd
			if( item->type() == crossType::AsinchroAnd )  
			{  
				Cross* myItem = (Cross*)item;  
				xmlWriter.writeStartElement("AsynchroAnd");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}

			// ��������� �������
			//---------------------------------
			// Line
			if( item->type() == arrowType::line )  
			{  
				Arrow* myItem = (Arrow*)item;  
				xmlWriter.writeStartElement("Line");  
				xmlWriter.writeAttribute("x1Coord", QString::number(myItem->getX1()));  
				xmlWriter.writeAttribute("y1Coord", QString::number(myItem->getY1()));
				xmlWriter.writeAttribute("x2Coord", QString::number(myItem->getX2()));  
				xmlWriter.writeAttribute("y2Coord", QString::number(myItem->getY2()));
				xmlWriter.writeAttribute("textArrow", myItem->getTextArrow());
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}

			// PointLine
			if( item->type() == arrowType::point )  
			{  
				Arrow* myItem = (Arrow*)item;  
				xmlWriter.writeStartElement("PointLine");  
				xmlWriter.writeAttribute("x1Coord", QString::number(myItem->getX1()));  
				xmlWriter.writeAttribute("y1Coord", QString::number(myItem->getY1()));
				xmlWriter.writeAttribute("x2Coord", QString::number(myItem->getX2()));  
				xmlWriter.writeAttribute("y2Coord", QString::number(myItem->getY2()));
				xmlWriter.writeAttribute("textArrow", myItem->getTextArrow());
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}

			// Double
			if( item->type() == arrowType::doubleLine )  
			{  
				Arrow* myItem = (Arrow*)item;  
				xmlWriter.writeStartElement("Double");  
				xmlWriter.writeAttribute("x1Coord", QString::number(myItem->getX1()));  
				xmlWriter.writeAttribute("y1Coord", QString::number(myItem->getY1()));
				xmlWriter.writeAttribute("x2Coord", QString::number(myItem->getX2()));  
				xmlWriter.writeAttribute("y2Coord", QString::number(myItem->getY2()));
				xmlWriter.writeAttribute("textArrow", myItem->getTextArrow());
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}
		}
		xmlWriter.writeEndElement();   //end of GraphicsItemList  
		xmlWriter.writeEndElement();   //end of SceneData  
		file.close();
	}
	ui.statusBar->showMessage(tr("Saving..."), 1000);
}

void MainWindow::actionSave()
{
	QFile file("NewDiagram.i3d");
	if(file.open(QIODevice::WriteOnly))		        //��������� ���� ������ ��� ������
	{
		QXmlStreamWriter xmlWriter(&file);
		xmlWriter.setAutoFormatting(true);
		xmlWriter.writeStartDocument();
		xmlWriter.writeStartElement("SceneData");
		xmlWriter.writeAttribute("version", "v1.0");
		xmlWriter.writeStartElement("GraphicsItemList");
		foreach( QGraphicsItem* item, scene->items())  
		{  
			int ct = item->type();

			// ��������� ������
			//------------------------------------
			if( item->type() == ElementType::work )  
			{  
				Work* myItem = (Work*)item;  
				xmlWriter.writeStartElement("Work");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeAttribute("number", myItem->getNumber());
				xmlWriter.writeAttribute("textWork", myItem->getText());
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			} 

			// ��������� ����������� �����������
			//----------------------------------
			// Xor
			if( item->type() == crossType::Xor )  
			{  
				Cross* myItem = (Cross*)item;  
				xmlWriter.writeStartElement("Xor");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}
			// SynchroOr
			if( item->type() == crossType::SinchroOr )  
			{  
				Cross* myItem = (Cross*)item;  
				xmlWriter.writeStartElement("SynchroOr");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}
			// SynchroAnd
			if( item->type() == crossType::SinchroAnd )  
			{  
				Cross* myItem = (Cross*)item;  
				xmlWriter.writeStartElement("SynchroAnd");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}
			// AsynchroOr
			if( item->type() == crossType::AsinchroOr )  
			{  
				Cross* myItem = (Cross*)item;  
				xmlWriter.writeStartElement("AsynchroOr");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}
			// AsynchroAnd
			if( item->type() == crossType::AsinchroAnd )  
			{  
				Cross* myItem = (Cross*)item;  
				xmlWriter.writeStartElement("AsynchroAnd");  
				xmlWriter.writeAttribute("xCoord", QString::number(myItem->x()));  
				xmlWriter.writeAttribute("yCoord", QString::number(myItem->y()));  
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}

			// ��������� �������
			//---------------------------------
			// Line
			if( item->type() == arrowType::line )  
			{  
				Arrow* myItem = (Arrow*)item;  
				xmlWriter.writeStartElement("Line");  
				xmlWriter.writeAttribute("x1Coord", QString::number(myItem->getX1()));  
				xmlWriter.writeAttribute("y1Coord", QString::number(myItem->getY1()));
				xmlWriter.writeAttribute("x2Coord", QString::number(myItem->getX2()));  
				xmlWriter.writeAttribute("y2Coord", QString::number(myItem->getY2()));
				xmlWriter.writeAttribute("textArrow", myItem->getTextArrow());
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}

			// PointLine
			if( item->type() == arrowType::point )  
			{  
				Arrow* myItem = (Arrow*)item;  
				xmlWriter.writeStartElement("PointLine");  
				xmlWriter.writeAttribute("x1Coord", QString::number(myItem->getX1()));  
				xmlWriter.writeAttribute("y1Coord", QString::number(myItem->getY1()));
				xmlWriter.writeAttribute("x2Coord", QString::number(myItem->getX2()));  
				xmlWriter.writeAttribute("y2Coord", QString::number(myItem->getY2()));
				xmlWriter.writeAttribute("textArrow", myItem->getTextArrow());
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}

			// Double
			if( item->type() == arrowType::doubleLine )  
			{  
				Arrow* myItem = (Arrow*)item;  
				xmlWriter.writeStartElement("Double");  
				xmlWriter.writeAttribute("x1Coord", QString::number(myItem->getX1()));  
				xmlWriter.writeAttribute("y1Coord", QString::number(myItem->getY1()));
				xmlWriter.writeAttribute("x2Coord", QString::number(myItem->getX2()));  
				xmlWriter.writeAttribute("y2Coord", QString::number(myItem->getY2()));
				xmlWriter.writeAttribute("textArrow", myItem->getTextArrow());
				xmlWriter.writeEndElement();  //end of MyGraphicsItem  
			}
		}
		xmlWriter.writeEndElement();   //end of GraphicsItemList  
		xmlWriter.writeEndElement();   //end of SceneData  
		file.close();
	}
	ui.statusBar->showMessage(tr("Saving..."), 1000);
}

void MainWindow::actionOpen()
{
	QMessageBox msgBox(this);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setWindowTitle(QString("Open file"));
	msgBox.setText(QString("Are you sure you want to open file?\nAll unsaved data will be lost."));
	QPushButton *yesButton = msgBox.addButton("Yes", QMessageBox::AcceptRole);
	QPushButton *noButton = msgBox.addButton("No", QMessageBox::RejectRole);
	msgBox.exec();
	if (msgBox.clickedButton() == yesButton)
	{
		QList <Block*> blocks;
		QList <Arrow*> arrows;
		QString FileName;
		FileName = QFileDialog::getOpenFileName (this, "Open file", "", "idef3 (*.i3d)");
		QFile file(FileName);				//������� ��������� �������������  ������
		/*QDir dir = QFileDialog.directory();
		QString dirName = dir.dirName();*/
		if(file.open(QIODevice::ReadOnly))  //��������� ���� ������ ��� ������
		{
			actionNew();
			QXmlStreamReader reader(&file);			//��������� ����� � ������
			while (!reader.atEnd())
			{
				if (reader.name() == "Work")
				{
					int xPos = reader.attributes().value("xCoord").toString().toInt();
					int yPos = reader.attributes().value("yCoord").toString().toInt();
					QString number = reader.attributes().value("number").toString();
					QString textWork = reader.attributes().value("textWork").toString();
					Work *work = new Work(NULL,this->scene->contextWorkMenu);
					work->setNumber(number);
					work->setText(textWork);
					blocks << work;
					this->scene->addItem(work);
					work->setPos(xPos,yPos);
					reader.readNext();
				}
				else if (reader.name() == "Xor")
				{
					int xPos = reader.attributes().value("xCoord").toString().toInt();
					int yPos = reader.attributes().value("yCoord").toString().toInt();
					Cross *xor = new Cross(NULL);
					xor->setCrossType(crossType::Xor);
					blocks << xor;
					this->scene->addItem(xor);
					xor->setPos(xPos,yPos);
					reader.readNext();
				}
				else if (reader.name() == "SynchroOr")
				{
					int xPos = reader.attributes().value("xCoord").toString().toInt();
					int yPos = reader.attributes().value("yCoord").toString().toInt();
					Cross *sor = new Cross(NULL);
					sor->setCrossType(crossType::SinchroOr);
					blocks << sor;
					this->scene->addItem(sor);
					sor->setPos(xPos,yPos);
					reader.readNext();
				}
				else if (reader.name() == "SynchroAnd")
				{
					int xPos = reader.attributes().value("xCoord").toString().toInt();
					int yPos = reader.attributes().value("yCoord").toString().toInt();
					Cross *sand = new Cross(NULL);
					sand->setCrossType(crossType::SinchroAnd);
					blocks << sand;
					this->scene->addItem(sand);
					sand->setPos(xPos,yPos);
					reader.readNext();
				}
				else if (reader.name() == "AsynchroOr")
				{
					int xPos = reader.attributes().value("xCoord").toString().toInt();
					int yPos = reader.attributes().value("yCoord").toString().toInt();
					Cross *aor = new Cross(NULL);
					aor->setCrossType(crossType::AsinchroOr);
					blocks << aor;
					this->scene->addItem(aor);
					aor->setPos(xPos,yPos);
					reader.readNext();
				}
				else if (reader.name() == "AsynchroAnd")
				{
					int xPos = reader.attributes().value("xCoord").toString().toInt();
					int yPos = reader.attributes().value("yCoord").toString().toInt();
					Cross *aand = new Cross(NULL);
					aand->setCrossType(crossType::AsinchroAnd);
					blocks << aand;
					this->scene->addItem(aand);
					aand->setPos(xPos,yPos);
					reader.readNext();
				}
				else if (reader.name() == "Line")
				{
					int x1Pos = reader.attributes().value("x1Coord").toString().toInt();
					int y1Pos = reader.attributes().value("y1Coord").toString().toInt();
					int x2Pos = reader.attributes().value("x2Coord").toString().toInt();
					int y2Pos = reader.attributes().value("y2Coord").toString().toInt();
					QString txt = reader.attributes().value("textArrow").toString();

					Block *f_i = qgraphicsitem_cast<Block*>(this->scene->itemAt(x1Pos, y1Pos));
					Block *s_i = qgraphicsitem_cast<Block*>(this->scene->itemAt(x2Pos, y2Pos));

					Arrow *line = new Arrow(NULL, this->ArrowContextMenu);
					line->setArrowType(arrowType::line);
					arrows << line;

					line->setFirstItem(f_i);
					line->setSecondItem(s_i);

					f_i->addArrow(line);
					s_i->addArrow(line);

					line->setTextArrow(txt);

					this->scene->addItem(line);
					line->update();
					reader.readNext();
				}
				else if (reader.name() == "PointLine")
				{
					int x1Pos = reader.attributes().value("x1Coord").toString().toInt();
					int y1Pos = reader.attributes().value("y1Coord").toString().toInt();
					int x2Pos = reader.attributes().value("x2Coord").toString().toInt();
					int y2Pos = reader.attributes().value("y2Coord").toString().toInt();
					QString txt = reader.attributes().value("textArrow").toString();

					Block *f_i = qgraphicsitem_cast<Block*>(this->scene->itemAt(x1Pos, y1Pos));
					Block *s_i = qgraphicsitem_cast<Block*>(this->scene->itemAt(x2Pos, y2Pos));

					Arrow *point = new Arrow(NULL, this->ArrowContextMenu);
					point->setArrowType(arrowType::point);
					arrows << point;

					f_i->addArrow(point);
					s_i->addArrow(point);

					point->setFirstItem(f_i);
					point->setSecondItem(s_i);

					point->setTextArrow(txt);

					this->scene->addItem(point);
					point->update();
					reader.readNext();
				}
				else if (reader.name() == "Double")
				{
					int x1Pos = reader.attributes().value("x1Coord").toString().toInt();
					int y1Pos = reader.attributes().value("y1Coord").toString().toInt();
					int x2Pos = reader.attributes().value("x2Coord").toString().toInt();
					int y2Pos = reader.attributes().value("y2Coord").toString().toInt();
					QString txt = reader.attributes().value("textArrow").toString();

					Block *f_i = qgraphicsitem_cast<Block*>(this->scene->itemAt(x1Pos, y1Pos));
					Block *s_i = qgraphicsitem_cast<Block*>(this->scene->itemAt(x2Pos, y2Pos));

					Arrow *doubleLine = new Arrow(NULL, this->ArrowContextMenu);
					doubleLine->setArrowType(arrowType::doubleLine);
					arrows << doubleLine;

					f_i->addArrow(doubleLine);
					s_i->addArrow(doubleLine);

					doubleLine->setFirstItem(f_i);
					doubleLine->setSecondItem(s_i);

					doubleLine->setTextArrow(txt);

					this->scene->addItem(doubleLine);
					doubleLine->update();
					reader.readNext();
				}
				reader.readNext();
			}
			file.close();
		}
		/*this->view->setScene(this->scene);
		this->view->setSceneRect(QRectF(0, 0, 3000, 1800));*/
		ui.statusBar->showMessage(tr("Opened file ") + FileName, 1000);
	}
}

void MainWindow::actionExport_to_PNG()
{
	QString FileName;
	FileName = QFileDialog::getSaveFileName (this, "Save file as...", "NewDiagram.png", "idef3 (*.png)");
	QPixmap image(scene->width(), scene->height());
	image.fill(Qt::white);
	QPainter painter(&image);
	scene->render(&painter);
	image.save(FileName);
	ui.statusBar->showMessage(tr("Exporting..."), 1000);
}

void MainWindow::actionExport_to_PNG_with()
{
	QString FileName;
	FileName = QFileDialog::getSaveFileName (this, "Save file as...", "NewDiagram.png", "idef3 (*.png)");
	QPixmap image(scene->width(), scene->height());
	QImage mark;
	mark.load(qApp->applicationDirPath() + "/images/watermark.png");
	image.fill(Qt::white);
	QPainter painter(&image);
	scene->render(&painter);
	painter.drawImage(QRectF(0, 0, scene->width(), scene->height()), mark);
	image.save(FileName);
	ui.statusBar->showMessage(tr("Exporting..."), 1000);
}

void MainWindow::actionExit()
{
	QMessageBox msgBox(this);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setWindowTitle(QString("Exit"));
	msgBox.setText(QString("Are you sure you want to quit?\nAll unsaved data will be lost."));
	QPushButton *yesButton = msgBox.addButton("Yes", QMessageBox::AcceptRole);
	QPushButton *noButton = msgBox.addButton("No", QMessageBox::RejectRole);
	msgBox.exec();
	if (msgBox.clickedButton() == yesButton)
		QMainWindow::close();
}

void MainWindow::closeEvent(QCloseEvent * event)
{
	QMessageBox msgBox(this);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setWindowTitle(QString("Exit"));
	msgBox.setText(QString("Are you sure you want to quit?\nAll unsaved data will be lost."));
	QPushButton *yesButton = msgBox.addButton("Yes", QMessageBox::AcceptRole);
	QPushButton *noButton = msgBox.addButton("No", QMessageBox::RejectRole);
	msgBox.exec();
	if (msgBox.clickedButton() == yesButton)
	{
		writeSettings();
		QMainWindow::close();
	}
	else
		event->ignore();
}

void MainWindow::createWorkContextMenu()
{
	QAction*fildContextMenu = new QAction(QString("edit"), this);
	connect(fildContextMenu, SIGNAL(triggered()), this, SLOT(editWorkContextMenuClick()));
	this->WorkContextMenu = new QMenu(this);
	this->WorkContextMenu->addAction(fildContextMenu);
}

void MainWindow::createArrowContextMenu()
{
	QAction*fildContextMenu = new QAction(QString("edit"), this);
	connect(fildContextMenu, SIGNAL(triggered()), this, SLOT(editArrowContextMenuClick()));
	this->ArrowContextMenu = new QMenu(this);
	this->ArrowContextMenu->addAction(fildContextMenu);
}

void MainWindow::addWork()
{
	ui.statusBar->showMessage(tr("Adding Work on diagram..."), 15000);
	this->scene->setActionType(ActionType::addWork);
}

void MainWindow::addCross1()
{
	ui.statusBar->showMessage(tr("Adding Exclusive OR on diagram..."), 15000);
	this->scene->setActionType(ActionType::addCross1);
}

void MainWindow::addCross2()
{
	ui.statusBar->showMessage(tr("Adding Synchronous OR on diagram..."), 15000);
	this->scene->setActionType(ActionType::addCross2);
}

void MainWindow::addCross3()
{
	ui.statusBar->showMessage(tr("Adding Synchronous AND on diagram..."), 15000);
	this->scene->setActionType(ActionType::addCross3);
}

void MainWindow::addCross4()
{
	ui.statusBar->showMessage(tr("Adding Asynchronous OR on diagram..."), 15000);
	this->scene->setActionType(ActionType::addCross4);
}

void MainWindow::addCross5()
{
	ui.statusBar->showMessage(tr("Adding Asynchronous AND on diagram..."), 15000);
	this->scene->setActionType(ActionType::addCross5);
}

void MainWindow::addArrow1()
{
	ui.statusBar->showMessage(tr("Adding Precedence on diagram..."), 15000);
	this->scene->setActionType(ActionType::addArrow1);
}

void MainWindow::addArrow2()
{
	ui.statusBar->showMessage(tr("Adding Relational Link on diagram..."), 15000);
	this->scene->setActionType(ActionType::addArrow2);
}

void MainWindow::addArrow3()
{
	ui.statusBar->showMessage(tr("Adding Object Flow on diagram..."), 15000);
	this->scene->setActionType(ActionType::addArrow3);
}

void MainWindow::editWorkContextMenuClick()
{
	this->scene->startEditingElement();
	QVector<QString>texts = this->scene->getTextsElement();
	if(QString("Work") == texts[0])
		this->textWork->Show(texts[1],texts[2]);
}

void MainWindow::editArrowContextMenuClick()
{
	this->scene->startEditingElement();
	QVector<QString>texts = this->scene->getTextsElement();
	if(QString("Line") == texts[0] || QString("Point") == texts[0] || QString("Double") == texts[0])
		this->textArrow->Show(texts[1]);
}

void MainWindow::addTextWork(QString txt, QString num)
{
	QVector<QString>texts;
	texts<<txt<<num;
	this->scene->addTextElement(texts);
}

void MainWindow::addTextArrow(QString txt)
{
	QVector<QString>texts;
	texts<<txt;
	this->scene->addTextElement(texts);
}

void MainWindow::scaleUp()
{
	ui.statusBar->showMessage(tr("Scale is Up"), 1000);
	this->view->scale(2, 2);
}

void MainWindow::scaleDown()
{
	ui.statusBar->showMessage(tr("Scale is Down"), 1000);
	this->view->scale(0.5, 0.5);
}