#include "textblock.h"

TextBlock::TextBlock(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	this->setModal(true);
	connect(ui.OK,SIGNAL(clicked()),this,SLOT(saveText()));
	connect(ui.Cancel,SIGNAL(clicked()),this,SLOT(cancel()));
}

TextBlock::~TextBlock()
{

}

void TextBlock::Show(QString text, QString number)
{
	ui.textInBlock->setText(text);
	ui.numberBlock->setValue(number.toInt());
	this->show();
}

void TextBlock::saveText()
{
	float num = ui.numberBlock->value();
	QString number;
	number.setNum(num);
	emit oktextblock(ui.textInBlock->text(),number);
	this->hide();
}

void TextBlock::cancel()
{
	this->hide();
}