/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Thu 17. Jan 05:55:52 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGraphicsView>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSplitter>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBox>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionNew;
    QAction *actionSave_as;
    QAction *actionExit;
    QAction *actionExport_to_GIF;
    QAction *actionExport_to_PNG_with;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QSplitter *splitter_2;
    QToolBox *toolBox;
    QWidget *block_page;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QPushButton *addCross4Btn;
    QPushButton *addCross5Btn;
    QPushButton *addWorkBtn;
    QPushButton *addCross1Btn;
    QPushButton *addCross2Btn;
    QPushButton *addCross3Btn;
    QWidget *arrow_page;
    QPushButton *addArrow2Btn;
    QPushButton *addArrow1Btn;
    QPushButton *addArrow3Btn;
    QWidget *page;
    QPushButton *scaleUpBtn;
    QLabel *label_5;
    QPushButton *scaleDownBtn;
    QGraphicsView *scene;
    QLabel *filename;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QString::fromUtf8("MainWindowClass"));
        MainWindowClass->resize(1000, 513);
        MainWindowClass->setMinimumSize(QSize(800, 480));
        actionOpen = new QAction(MainWindowClass);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon;
        icon.addFile(QString::fromUtf8("images/open.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon);
        actionSave = new QAction(MainWindowClass);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("images/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave->setIcon(icon1);
        actionNew = new QAction(MainWindowClass);
        actionNew->setObjectName(QString::fromUtf8("actionNew"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("images/new.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNew->setIcon(icon2);
        actionSave_as = new QAction(MainWindowClass);
        actionSave_as->setObjectName(QString::fromUtf8("actionSave_as"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8("images/saveas.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave_as->setIcon(icon3);
        actionExit = new QAction(MainWindowClass);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8("images/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon4);
        actionExport_to_GIF = new QAction(MainWindowClass);
        actionExport_to_GIF->setObjectName(QString::fromUtf8("actionExport_to_GIF"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8("images/export.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExport_to_GIF->setIcon(icon5);
        actionExport_to_PNG_with = new QAction(MainWindowClass);
        actionExport_to_PNG_with->setObjectName(QString::fromUtf8("actionExport_to_PNG_with"));
        actionExport_to_PNG_with->setIcon(icon5);
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        splitter_2 = new QSplitter(centralWidget);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        toolBox = new QToolBox(splitter_2);
        toolBox->setObjectName(QString::fromUtf8("toolBox"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(toolBox->sizePolicy().hasHeightForWidth());
        toolBox->setSizePolicy(sizePolicy);
        toolBox->setMinimumSize(QSize(150, 430));
        block_page = new QWidget();
        block_page->setObjectName(QString::fromUtf8("block_page"));
        block_page->setGeometry(QRect(0, 0, 207, 349));
        label = new QLabel(block_page);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(9, 9, 49, 23));
        QFont font;
        font.setPointSize(14);
        label->setFont(font);
        label_2 = new QLabel(block_page);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(9, 102, 69, 23));
        label_2->setFont(font);
        label_3 = new QLabel(block_page);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(9, 195, 81, 16));
        QFont font1;
        font1.setPointSize(10);
        label_3->setFont(font1);
        label_4 = new QLabel(block_page);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(9, 281, 91, 16));
        label_4->setFont(font1);
        addCross4Btn = new QPushButton(block_page);
        addCross4Btn->setObjectName(QString::fromUtf8("addCross4Btn"));
        addCross4Btn->setGeometry(QRect(9, 303, 51, 50));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8("images/aOr.png"), QSize(), QIcon::Normal, QIcon::Off);
        addCross4Btn->setIcon(icon6);
        addCross4Btn->setIconSize(QSize(49, 46));
        addCross5Btn = new QPushButton(block_page);
        addCross5Btn->setObjectName(QString::fromUtf8("addCross5Btn"));
        addCross5Btn->setGeometry(QRect(84, 303, 51, 50));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8("images/aAnd.png"), QSize(), QIcon::Normal, QIcon::Off);
        addCross5Btn->setIcon(icon7);
        addCross5Btn->setIconSize(QSize(49, 46));
        addWorkBtn = new QPushButton(block_page);
        addWorkBtn->setObjectName(QString::fromUtf8("addWorkBtn"));
        addWorkBtn->setGeometry(QRect(9, 38, 51, 42));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8("images/work.png"), QSize(), QIcon::Normal, QIcon::Off);
        addWorkBtn->setIcon(icon8);
        addWorkBtn->setIconSize(QSize(50, 40));
        addWorkBtn->setCheckable(false);
        addWorkBtn->setChecked(false);
        addCross1Btn = new QPushButton(block_page);
        addCross1Btn->setObjectName(QString::fromUtf8("addCross1Btn"));
        addCross1Btn->setGeometry(QRect(9, 131, 51, 50));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8("images/xor.png"), QSize(), QIcon::Normal, QIcon::Off);
        addCross1Btn->setIcon(icon9);
        addCross1Btn->setIconSize(QSize(49, 46));
        addCross1Btn->setCheckable(false);
        addCross1Btn->setChecked(false);
        addCross2Btn = new QPushButton(block_page);
        addCross2Btn->setObjectName(QString::fromUtf8("addCross2Btn"));
        addCross2Btn->setGeometry(QRect(9, 217, 51, 50));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8("images/sOr.png"), QSize(), QIcon::Normal, QIcon::Off);
        addCross2Btn->setIcon(icon10);
        addCross2Btn->setIconSize(QSize(49, 46));
        addCross2Btn->setCheckable(false);
        addCross2Btn->setChecked(false);
        addCross3Btn = new QPushButton(block_page);
        addCross3Btn->setObjectName(QString::fromUtf8("addCross3Btn"));
        addCross3Btn->setGeometry(QRect(84, 217, 51, 50));
        QIcon icon11;
        icon11.addFile(QString::fromUtf8("images/sAnd.png"), QSize(), QIcon::Normal, QIcon::Off);
        addCross3Btn->setIcon(icon11);
        addCross3Btn->setIconSize(QSize(49, 46));
        addCross3Btn->setCheckable(false);
        addCross3Btn->setChecked(false);
        toolBox->addItem(block_page, QString::fromUtf8("Blocks"));
        arrow_page = new QWidget();
        arrow_page->setObjectName(QString::fromUtf8("arrow_page"));
        arrow_page->setGeometry(QRect(0, 0, 207, 349));
        addArrow2Btn = new QPushButton(arrow_page);
        addArrow2Btn->setObjectName(QString::fromUtf8("addArrow2Btn"));
        addArrow2Btn->setGeometry(QRect(40, 50, 60, 42));
        addArrow2Btn->setMinimumSize(QSize(60, 40));
        QIcon icon12;
        icon12.addFile(QString::fromUtf8("images/dash.png"), QSize(), QIcon::Normal, QIcon::Off);
        addArrow2Btn->setIcon(icon12);
        addArrow2Btn->setIconSize(QSize(55, 38));
        addArrow1Btn = new QPushButton(arrow_page);
        addArrow1Btn->setObjectName(QString::fromUtf8("addArrow1Btn"));
        addArrow1Btn->setGeometry(QRect(40, 0, 60, 42));
        addArrow1Btn->setMinimumSize(QSize(60, 40));
        QIcon icon13;
        icon13.addFile(QString::fromUtf8("images/line.png"), QSize(), QIcon::Normal, QIcon::Off);
        addArrow1Btn->setIcon(icon13);
        addArrow1Btn->setIconSize(QSize(55, 38));
        addArrow3Btn = new QPushButton(arrow_page);
        addArrow3Btn->setObjectName(QString::fromUtf8("addArrow3Btn"));
        addArrow3Btn->setGeometry(QRect(40, 100, 60, 42));
        addArrow3Btn->setMinimumSize(QSize(60, 40));
        QIcon icon14;
        icon14.addFile(QString::fromUtf8("images/double.png"), QSize(), QIcon::Normal, QIcon::Off);
        addArrow3Btn->setIcon(icon14);
        addArrow3Btn->setIconSize(QSize(55, 38));
        toolBox->addItem(arrow_page, QString::fromUtf8("Arrows"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        page->setGeometry(QRect(0, 0, 207, 349));
        scaleUpBtn = new QPushButton(page);
        scaleUpBtn->setObjectName(QString::fromUtf8("scaleUpBtn"));
        scaleUpBtn->setGeometry(QRect(0, 30, 44, 44));
        QIcon icon15;
        icon15.addFile(QString::fromUtf8("images/plus.png"), QSize(), QIcon::Normal, QIcon::Off);
        scaleUpBtn->setIcon(icon15);
        scaleUpBtn->setIconSize(QSize(40, 40));
        label_5 = new QLabel(page);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(0, 0, 51, 21));
        label_5->setFont(font);
        scaleDownBtn = new QPushButton(page);
        scaleDownBtn->setObjectName(QString::fromUtf8("scaleDownBtn"));
        scaleDownBtn->setGeometry(QRect(50, 30, 44, 44));
        QIcon icon16;
        icon16.addFile(QString::fromUtf8("images/minus.png"), QSize(), QIcon::Normal, QIcon::Off);
        scaleDownBtn->setIcon(icon16);
        scaleDownBtn->setIconSize(QSize(40, 40));
        toolBox->addItem(page, QString::fromUtf8("Tools"));
        splitter_2->addWidget(toolBox);
        scene = new QGraphicsView(splitter_2);
        scene->setObjectName(QString::fromUtf8("scene"));
        splitter_2->addWidget(scene);

        gridLayout->addWidget(splitter_2, 0, 0, 1, 1);

        filename = new QLabel(centralWidget);
        filename->setObjectName(QString::fromUtf8("filename"));

        gridLayout->addWidget(filename, 1, 0, 1, 1);

        MainWindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1000, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        MainWindowClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindowClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindowClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addSeparator();
        menuFile->addAction(actionSave);
        menuFile->addAction(actionSave_as);
        menuFile->addSeparator();
        menuFile->addAction(actionExport_to_GIF);
        menuFile->addAction(actionExport_to_PNG_with);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);

        retranslateUi(MainWindowClass);

        toolBox->setCurrentIndex(0);
        toolBox->layout()->setSpacing(6);


        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QApplication::translate("MainWindowClass", "idef3_editor_2.0", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MainWindowClass", "Open", 0, QApplication::UnicodeUTF8));
        actionSave->setText(QApplication::translate("MainWindowClass", "Save", 0, QApplication::UnicodeUTF8));
        actionNew->setText(QApplication::translate("MainWindowClass", "New", 0, QApplication::UnicodeUTF8));
        actionSave_as->setText(QApplication::translate("MainWindowClass", "Save as...", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindowClass", "Exit", 0, QApplication::UnicodeUTF8));
        actionExport_to_GIF->setText(QApplication::translate("MainWindowClass", "Export to PNG", 0, QApplication::UnicodeUTF8));
        actionExport_to_PNG_with->setText(QApplication::translate("MainWindowClass", "Export to PNG with watermark", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindowClass", "Work:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindowClass", "Crosses:", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindowClass", "Synchronous:", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindowClass", "Asynchronous:", 0, QApplication::UnicodeUTF8));
        addCross4Btn->setText(QString());
        addCross5Btn->setText(QString());
        addWorkBtn->setText(QString());
        addCross1Btn->setText(QString());
        addCross2Btn->setText(QString());
        addCross3Btn->setText(QString());
        toolBox->setItemText(toolBox->indexOf(block_page), QApplication::translate("MainWindowClass", "Blocks", 0, QApplication::UnicodeUTF8));
        addArrow2Btn->setText(QString());
        addArrow1Btn->setText(QString());
        addArrow3Btn->setText(QString());
        toolBox->setItemText(toolBox->indexOf(arrow_page), QApplication::translate("MainWindowClass", "Arrows", 0, QApplication::UnicodeUTF8));
        scaleUpBtn->setText(QString());
        label_5->setText(QApplication::translate("MainWindowClass", "Scale:", 0, QApplication::UnicodeUTF8));
        scaleDownBtn->setText(QString());
        toolBox->setItemText(toolBox->indexOf(page), QApplication::translate("MainWindowClass", "Tools", 0, QApplication::UnicodeUTF8));
        filename->setText(QString());
        menuFile->setTitle(QApplication::translate("MainWindowClass", "File", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
