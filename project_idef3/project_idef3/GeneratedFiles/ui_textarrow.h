/********************************************************************************
** Form generated from reading UI file 'textarrow.ui'
**
** Created: Thu 17. Jan 05:55:55 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEXTARROW_H
#define UI_TEXTARROW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_TextArrow
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *comment;
    QPushButton *OK;
    QPushButton *Cancel;

    void setupUi(QDialog *TextArrow)
    {
        if (TextArrow->objectName().isEmpty())
            TextArrow->setObjectName(QString::fromUtf8("TextArrow"));
        TextArrow->resize(250, 90);
        gridLayout = new QGridLayout(TextArrow);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(TextArrow);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        comment = new QLineEdit(TextArrow);
        comment->setObjectName(QString::fromUtf8("comment"));
        comment->setMaxLength(32768);

        gridLayout->addWidget(comment, 1, 0, 1, 2);

        OK = new QPushButton(TextArrow);
        OK->setObjectName(QString::fromUtf8("OK"));

        gridLayout->addWidget(OK, 2, 0, 1, 1);

        Cancel = new QPushButton(TextArrow);
        Cancel->setObjectName(QString::fromUtf8("Cancel"));

        gridLayout->addWidget(Cancel, 2, 1, 1, 1);


        retranslateUi(TextArrow);

        QMetaObject::connectSlotsByName(TextArrow);
    } // setupUi

    void retranslateUi(QDialog *TextArrow)
    {
        TextArrow->setWindowTitle(QApplication::translate("TextArrow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\272\320\276\320\274\320\274\320\265\320\275\321\202\320\260\321\200\320\270\320\271 \320\272 \321\201\321\202\321\200\320\265\320\273\320\272\320\265", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("TextArrow", "Comment", 0, QApplication::UnicodeUTF8));
        OK->setText(QApplication::translate("TextArrow", "OK", 0, QApplication::UnicodeUTF8));
        Cancel->setText(QApplication::translate("TextArrow", "Cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TextArrow: public Ui_TextArrow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEXTARROW_H
