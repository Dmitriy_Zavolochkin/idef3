/********************************************************************************
** Form generated from reading UI file 'textblock.ui'
**
** Created: Thu 17. Jan 05:55:55 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEXTBLOCK_H
#define UI_TEXTBLOCK_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QSplitter>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TextBlock
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QSpinBox *numberBlock;
    QLabel *label_2;
    QLineEdit *textInBlock;
    QSplitter *splitter_3;
    QPushButton *OK;
    QPushButton *Cancel;

    void setupUi(QWidget *TextBlock)
    {
        if (TextBlock->objectName().isEmpty())
            TextBlock->setObjectName(QString::fromUtf8("TextBlock"));
        TextBlock->resize(250, 110);
        TextBlock->setMinimumSize(QSize(250, 110));
        TextBlock->setMaximumSize(QSize(253, 300));
        gridLayout = new QGridLayout(TextBlock);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(TextBlock);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        numberBlock = new QSpinBox(TextBlock);
        numberBlock->setObjectName(QString::fromUtf8("numberBlock"));

        gridLayout->addWidget(numberBlock, 0, 1, 1, 1);

        label_2 = new QLabel(TextBlock);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        textInBlock = new QLineEdit(TextBlock);
        textInBlock->setObjectName(QString::fromUtf8("textInBlock"));
        textInBlock->setMaxLength(32768);

        gridLayout->addWidget(textInBlock, 2, 0, 1, 2);

        splitter_3 = new QSplitter(TextBlock);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setOrientation(Qt::Horizontal);
        OK = new QPushButton(splitter_3);
        OK->setObjectName(QString::fromUtf8("OK"));
        splitter_3->addWidget(OK);
        Cancel = new QPushButton(splitter_3);
        Cancel->setObjectName(QString::fromUtf8("Cancel"));
        splitter_3->addWidget(Cancel);

        gridLayout->addWidget(splitter_3, 3, 0, 1, 2);


        retranslateUi(TextBlock);

        QMetaObject::connectSlotsByName(TextBlock);
    } // setupUi

    void retranslateUi(QWidget *TextBlock)
    {
        TextBlock->setWindowTitle(QApplication::translate("TextBlock", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\201\320\276\320\264\320\265\321\200\320\266\320\270\320\274\320\276\320\265 \320\261\320\273\320\276\320\272\320\260", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("TextBlock", "Number of block", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("TextBlock", "Text", 0, QApplication::UnicodeUTF8));
        OK->setText(QApplication::translate("TextBlock", "OK", 0, QApplication::UnicodeUTF8));
        Cancel->setText(QApplication::translate("TextBlock", "Cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TextBlock: public Ui_TextBlock {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEXTBLOCK_H
