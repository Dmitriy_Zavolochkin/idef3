#include "work.h"

Work::Work(QGraphicsItem *parent, QMenu *menu)
	: Block(parent, menu)
{
	this->setFlag(QGraphicsItem::ItemIsMovable);
	this->setFlag(QGraphicsItem::ItemIsSelectable);

	Block::setElementType(ElementType::work);
	Block::setTextType(ElementType::work);
	this->Number = Number;
	this->TextWork = TextWork;
	this->contextMenu = menu;
}

Work::~Work()
{

}

void Work::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0)
{
	painter->setRenderHint(QPainter::Antialiasing, true);
	painter->setBrush(Qt::white);
	
	if(this->isSelected())
	{
		painter->setPen(QPen(Qt::red));
		this->setZValue(500);
	}
	else
	{
		this->setZValue(0);
		painter->setPen(QPen(Qt::black));
	}

	QRectF rect = this->boundingRect();

	int x = rect.x(), y = rect.y(), h = y+rect.height(), w = x+rect.width();

	//������ ������
	painter->drawRect(rect);
	painter->drawLine(x, y+51, w, y+51);
	painter->drawLine(0, y+51, 0, y+76);
	painter->drawText(QRectF(x+10,0,w-20,h+10),Number,QTextOption(Qt::AlignCenter));
	painter->drawText(x+10,y+20,w+w-20,h+20,Qt::AlignLeft,TextWork,0);
}

QRectF Work::boundingRect() const 
{
	return QRectF(-85, -38, 170, 76);
}