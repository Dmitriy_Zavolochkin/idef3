#ifndef CROSS_H
#define CROSS_H

#include "block.h"
#include <QImage>
#include <QApplication>

enum crossType { Xor = 65536, SinchroOr = 65537, SinchroAnd = 65538, AsinchroOr = 65539, AsinchroAnd = 65540 };

class Cross : public Block
{
public:
	Cross(QGraphicsItem *parent);
	~Cross();

	// ��������� �����������
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	// �������� �������, ������� ������������
	QRectF boundingRect() const;

	// ������ ��� �����������
	void setCrossType(crossType type) {
		this->cType = type;
	}

	int type() const {
		return this->cType;
	}

	crossType getType() {
		return this->cType;
	}

private:
	crossType cType;
	QImage *xorImage, *sorImage, *aorImage, *sandImage, *aandImage;
};

#endif // CROSS_H
