#include "arrow.h"

Arrow::Arrow(QGraphicsItem *parent, QMenu *menu)
	: QGraphicsLineItem(parent)
{
	this->setFlag(QGraphicsItem::ItemIsSelectable);
	this->setZValue(-1);
	this->textArrow = textArrow;
	this->contextMenu = menu;

	first = NULL;
	second = NULL;

	arrowHeadRight = new QImage(qApp->applicationDirPath() + "/crossImages/arrowHeadRight.png");
	arrowHeadLeft = new QImage(qApp->applicationDirPath() + "/crossImages/arrowHeadLeft.png");
	arrowHeadUp = new QImage(qApp->applicationDirPath() + "/crossImages/arrowHeadUp.png");
	arrowHeadDown = new QImage(qApp->applicationDirPath() + "/crossImages/arrowHeadDown.png");
}

Arrow::Arrow(QGraphicsItem *parent, QGraphicsItem *f, QMenu *menu)
	: QGraphicsLineItem(parent)
{
	this->setFlag(QGraphicsItem::ItemIsSelectable);
	this->setZValue(-1);
	this->contextMenu = menu;

	first = f;
	second = NULL;

	secondPoint = first->scenePos();

	arrowHeadRight = new QImage(qApp->applicationDirPath() + "/crossImages/arrowHeadRight.png");
	arrowHeadLeft = new QImage(qApp->applicationDirPath() + "/crossImages/arrowHeadLeft.png");
	arrowHeadUp = new QImage(qApp->applicationDirPath() + "/crossImages/arrowHeadUp.png");
	arrowHeadDown = new QImage(qApp->applicationDirPath() + "/crossImages/arrowHeadDown.png");
}

Arrow::~Arrow()
{

}

void Arrow::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	if(first == NULL)
		return;
	
	if(this->isSelected())
		painter->setPen(Qt::red);
	else
		painter->setPen(Qt::black);
	
	if(second == NULL)
	{
		painter->drawLine(first->pos(), secondPoint);
	}
	else
	{
		float x1 = first->scenePos().x(), y1 = first->scenePos().y(),
			  x2 = second->scenePos().x(), y2 = second->scenePos().y();

		QRectF first_rect = first->boundingRect();
		QRectF second_rect = second->boundingRect();

		float second_w = second_rect.width();
		float second_h = second_rect.height();
		float first_w = first_rect.width();
		float first_h = first_rect.height();
		
		float X = x2-x1, Y = y2-y1;

		if(aType == arrowType::line)
		{
			if(abs(X) > abs(Y))
			{
				painter->drawLine(x1, y1, x1+X/2, y1);
				painter->drawLine(x1+X/2, y1, x1+X/2, y2);
				painter->drawLine(x1+X/2, y2, x2, y2);
				if(x2 > x1)
				{
					
					painter->drawImage(x2-10-second_w/2, y2-3, *arrowHeadRight);
					if (!textArrow.isEmpty())
					{
						if(y1 > y2)
						{
							painter->drawText(x1+X/2+20, y2-50, 50, 20, Qt::AlignCenter, textArrow, 0);
							painter->drawLine(x1+X/2, y2, x1+X/2+10+25, y2-50+30);
							painter->drawLine(x1+X/2+10+25, y2-50+30, x1+X/2, y2-50+30);
							painter->drawLine(x1+X/2, y2-50+30, x1+X/2+20, y2-50+20);
						}
						else
						{
							painter->drawText(x1+X/2+20, y1-50, textArrow);
							painter->drawLine(x1+X/2, y1, x1+X/2+10+25, y1-50+30);
							painter->drawLine(x1+X/2+10+25, y1-50+30, x1+X/2, y1-50+30);
							painter->drawLine(x1+X/2, y1-50+30, x1+X/2+20, y1-50+20);
						}
					}
				}
				else
				{
					painter->drawImage(x2+second_w/2, y2-3, *arrowHeadLeft);
					if (!textArrow.isEmpty())
					{
						if(y1 > y2)
						{
							painter->drawText(x2-X/2-40, y2-50, textArrow);
							painter->drawLine(x2-X/2, y2, x2-X/2-10-25, y2-50+30);
							painter->drawLine(x2-X/2-10-25, y2-50+30, x2-X/2, y2-50+30);
							painter->drawLine(x2-X/2, y2-50+30, x2-X/2-10-25, y2-50+10);
						}
						else
						{
							painter->drawText(x2-X/2-40, y1-50, textArrow);
							painter->drawLine(x1+X/2, y1, x1+X/2-10-25, y1-50+30);
							painter->drawLine(x1+X/2-10-25, y1-50+30, x1+X/2, y1-50+30);
							painter->drawLine(x1+X/2, y1-50+30, x1+X/2-10-25, y1-50+10);
						}
					}
				}
			}
			else
			{
				painter->drawLine(x1, y1, x1, y1+Y/2);
				painter->drawLine(x1, y1+Y/2, x2, y1+Y/2);
				painter->drawLine(x2, y1+Y/2, x2, y2);
				
				if(y2 < y1)
				{
					painter->drawImage(x2-3, y2+second_h/2, *arrowHeadUp);
					if (!textArrow.isEmpty())
					{
						if(x1 > x2)
						{
							painter->drawText(x1+20, y2-Y/2-50, textArrow);
							painter->drawLine(x1, y1+Y/2, x1+35, y1+Y/2-20);
							painter->drawLine(x1+35, y1+Y/2-20, x1, y1+Y/2-20);
							painter->drawLine(x1, y1+Y/2-20, x1+35, y1+Y/2-40);
						}
						else
						{
							painter->drawText(x2+20, y2-Y/2-50, textArrow);
							painter->drawLine(x2, y2-Y/2, x2+35, y2-Y/2-20);
							painter->drawLine(x2+35, y2-Y/2-20, x2+5, y2-Y/2-20);
							painter->drawLine(x2+5, y2-Y/2-20, x2+35, y2-Y/2-40);
						}
					}
				}
				else
				{
					painter->drawImage(x2-3, y2-second_h/2-10, *arrowHeadDown);
					if (!textArrow.isEmpty())
					{
						if(x1 > x2)
						{
							painter->drawText(x1+20, y1+Y/2-50, textArrow);
							painter->drawLine(x1, y1+Y/2, x1+35, y1+Y/2-20);
							painter->drawLine(x1+35, y1+Y/2-20, x1+5, y1+Y/2-20);
							painter->drawLine(x1+5, y1+Y/2-20, x1+35, y1+Y/2-40);
						}
						else
						{
							painter->drawText(x2+20, y1+Y/2-50, textArrow);
							painter->drawLine(x2, y2-Y/2, x2+35, y2-Y/2-20);
							painter->drawLine(x2+35, y2-Y/2-20, x2+5, y2-Y/2-20);
							painter->drawLine(x2+5, y2-Y/2-20, x2+35, y2-Y/2-40);
						}
					}
				}
			}
		}
		else if(aType == arrowType::point)
		{
			painter->setPen(Qt::DotLine);
			if(abs(X) > abs(Y))
			{
				painter->drawLine(x1, y1, x1+X/2, y1);
				painter->drawLine(x1+X/2, y1, x1+X/2, y2);
				painter->drawLine(x1+X/2, y2, x2, y2);
				
				if(x2 > x1)
				{
					painter->drawImage(x2-10-second_w/2, y2-3, *arrowHeadRight);
					if (!textArrow.isEmpty())
					{
						painter->setPen(Qt::SolidLine);
						if(y1 > y2)
						{
							painter->drawText(x1+X/2+20, y2-50, textArrow);
							painter->drawLine(x1+X/2, y2, x1+X/2+10+25, y2-50+30);
							painter->drawLine(x1+X/2+10+25, y2-50+30, x1+X/2, y2-50+30);
							painter->drawLine(x1+X/2, y2-50+30, x1+X/2+20, y2-50+20);
						}
						else
						{
							painter->drawText(x1+X/2+20, y1-50, textArrow);
							painter->drawLine(x1+X/2, y1, x1+X/2+10+25, y1-50+30);
							painter->drawLine(x1+X/2+10+25, y1-50+30, x1+X/2, y1-50+30);
							painter->drawLine(x1+X/2, y1-50+30, x1+X/2+20, y1-50+20);
						}
					}
				}
				else
				{
					painter->drawImage(x2+second_w/2, y2-3, *arrowHeadLeft);
					if (!textArrow.isEmpty())
					{
						painter->setPen(Qt::SolidLine);
						if(y1 > y2)
						{
							painter->drawText(x2-X/2-40, y2-50, textArrow);
							painter->drawLine(x2-X/2, y2, x2-X/2-10-25, y2-50+30);
							painter->drawLine(x2-X/2-10-25, y2-50+30, x2-X/2, y2-50+30);
							painter->drawLine(x2-X/2, y2-50+30, x2-X/2-10-25, y2-50+10);
						}
						else
						{
							painter->drawText(x2-X/2-40, y1-50, textArrow);
							painter->drawLine(x1+X/2, y1, x1+X/2-10-25, y1-50+30);
							painter->drawLine(x1+X/2-10-25, y1-50+30, x1+X/2, y1-50+30);
							painter->drawLine(x1+X/2, y1-50+30, x1+X/2-10-25, y1-50+10);
						}
					}
				}
			}
			else
			{
				painter->drawLine(x1, y1 , x1, y1+Y/2);
				painter->drawLine(x1, y1+Y/2, x2, y1+Y/2);
				painter->drawLine(x2, y1+Y/2, x2, y2);
				
				if(y2 < y1)
				{
					painter->drawImage(x2-3, y2+second_h/2, *arrowHeadUp);
					if (!textArrow.isEmpty())
					{
						painter->setPen(Qt::SolidLine);
						if(x1 > x2)
						{
							painter->drawText(x1+20, y2-Y/2-50, textArrow);
							painter->drawLine(x1, y1+Y/2, x1+35, y1+Y/2-20);
							painter->drawLine(x1+35, y1+Y/2-20, x1, y1+Y/2-20);
							painter->drawLine(x1, y1+Y/2-20, x1+35, y1+Y/2-40);
						}
						else
						{
							painter->drawText(x2+20, y2-Y/2-50, textArrow);
							painter->drawLine(x2, y2-Y/2, x2+35, y2-Y/2-20);
							painter->drawLine(x2+35, y2-Y/2-20, x2+5, y2-Y/2-20);
							painter->drawLine(x2+5, y2-Y/2-20, x2+35, y2-Y/2-40);
						}
					}
				}
				else
				{
					painter->drawImage(x2-3, y2-second_h/2-10, *arrowHeadDown);
					if (!textArrow.isEmpty())
					{
						painter->setPen(Qt::SolidLine);
						if(x1 > x2)
						{
							painter->drawText(x1+20, y1+Y/2-50, textArrow);
							painter->drawLine(x1, y1+Y/2, x1+35, y1+Y/2-20);
							painter->drawLine(x1+35, y1+Y/2-20, x1+5, y1+Y/2-20);
							painter->drawLine(x1+5, y1+Y/2-20, x1+35, y1+Y/2-40);
						}
						else
						{
							painter->drawText(x2+20, y1+Y/2-50, textArrow);
							painter->drawLine(x2, y2-Y/2, x2+35, y2-Y/2-20);
							painter->drawLine(x2+35, y2-Y/2-20, x2+5, y2-Y/2-20);
							painter->drawLine(x2+5, y2-Y/2-20, x2+35, y2-Y/2-40);
						}
					}
				}
			}
		}
		else if(aType == arrowType::doubleLine)
		{
			if(abs(X) > abs(Y))
			{
				painter->drawLine(x1, y1, x1+X/2, y1);
				painter->drawLine(x1+X/2, y1, x1+X/2, y2);
				painter->drawLine(x1+X/2, y2, x2, y2);

				if(x2 > x1)
				{
					painter->drawImage(x2-10-second_w/2, y2-3, *arrowHeadRight);
					painter->drawImage(x2-20-second_w/2, y2-3, *arrowHeadRight);
					if (!textArrow.isEmpty())
					{
						if(y1 > y2)
						{
							painter->drawText(x1+X/2+20, y2-50, textArrow);
							painter->drawLine(x1+X/2, y2, x1+X/2+10+25, y2-50+30);
							painter->drawLine(x1+X/2+10+25, y2-50+30, x1+X/2, y2-50+30);
							painter->drawLine(x1+X/2, y2-50+30, x1+X/2+20, y2-50+20);
						}
						else
						{
							painter->drawText(x1+X/2+20, y1-50, textArrow);
							painter->drawLine(x1+X/2, y1, x1+X/2+10+25, y1-50+30);
							painter->drawLine(x1+X/2+10+25, y1-50+30, x1+X/2, y1-50+30);
							painter->drawLine(x1+X/2, y1-50+30, x1+X/2+20, y1-50+20);
						}
					}
				}
				else
				{
					painter->drawImage(x2+second_w/2, y2-3, *arrowHeadLeft);
					painter->drawImage(x2+10+second_w/2, y2-3, *arrowHeadLeft);
					if (!textArrow.isEmpty())
					{
						if(y1 > y2)
						{
							painter->drawText(x2-X/2-40, y2-50, textArrow);
							painter->drawLine(x2-X/2, y2, x2-X/2-10-25, y2-50+30);
							painter->drawLine(x2-X/2-10-25, y2-50+30, x2-X/2, y2-50+30);
							painter->drawLine(x2-X/2, y2-50+30, x2-X/2-10-25, y2-50+10);
						}
						else
						{
							painter->drawText(x2-X/2-40, y1-50, textArrow);
							painter->drawLine(x1+X/2, y1, x1+X/2-10-25, y1-50+30);
							painter->drawLine(x1+X/2-10-25, y1-50+30, x1+X/2, y1-50+30);
							painter->drawLine(x1+X/2, y1-50+30, x1+X/2-10-25, y1-50+10);
						}
					}
				}
			}
			else
			{
				painter->drawLine(x1, y1, x1, y1+Y/2);
				painter->drawLine(x1, y1+Y/2, x2, y1+Y/2);
				painter->drawLine(x2, y1+Y/2, x2, y2);

				if(y2 < y1)
				{
					painter->drawImage(x2-3, y2+second_h/2, *arrowHeadUp);
					painter->drawImage(x2-3, y2+10+second_h/2, *arrowHeadUp);
					if (!textArrow.isEmpty())
					{
						if(x1 > x2)
						{
							painter->drawText(x1+20, y2-Y/2-50, textArrow);
							painter->drawLine(x1, y1+Y/2, x1+35, y1+Y/2-20);
							painter->drawLine(x1+35, y1+Y/2-20, x1, y1+Y/2-20);
							painter->drawLine(x1, y1+Y/2-20, x1+35, y1+Y/2-40);
						}
						else
						{
							painter->drawText(x2+20, y2-Y/2-50, textArrow);
							painter->drawLine(x2, y2-Y/2, x2+35, y2-Y/2-20);
							painter->drawLine(x2+35, y2-Y/2-20, x2+5, y2-Y/2-20);
							painter->drawLine(x2+5, y2-Y/2-20, x2+35, y2-Y/2-40);
						}
					}
				}
				else
				{
					painter->drawImage(x2-3, y2-second_h/2-10, *arrowHeadDown);
					painter->drawImage(x2-3, y2-20-second_h/2, *arrowHeadDown);
					if (!textArrow.isEmpty())
					{
						if(x1 > x2)
						{
							painter->drawText(x1+20, y1+Y/2-50, textArrow);
							painter->drawLine(x1, y1+Y/2, x1+35, y1+Y/2-20);
							painter->drawLine(x1+35, y1+Y/2-20, x1+5, y1+Y/2-20);
							painter->drawLine(x1+5, y1+Y/2-20, x1+35, y1+Y/2-40);
						}
						else
						{
							painter->drawText(x2+20, y1+Y/2-50, textArrow);
							painter->drawLine(x2, y2-Y/2, x2+35, y2-Y/2-20);
							painter->drawLine(x2+35, y2-Y/2-20, x2+5, y2-Y/2-20);
							painter->drawLine(x2+5, y2-Y/2-20, x2+35, y2-Y/2-40);
						}
					}
				}
			}
		}
	}
}

void Arrow::update()
{
	if(first == NULL)
		return;
	if(second == NULL)
		setLine(QLineF(first->pos(), secondPoint));
	else
		setLine(QLineF(first->pos(), second->pos()));
}

void Arrow::updateToPoint(QPointF point)
{
	secondPoint = point;
	update();
}

QRectF Arrow::boundingRect() const
{
	QRectF r = QGraphicsLineItem::boundingRect();
	r.setTopLeft(r.topLeft() + QPointF(-7.0, -7.0));
	r.setBottomRight(r.bottomRight() + QPointF(7.0, 7.0));
	return r;
}

QPainterPath Arrow::shape() const
{
	QPainterPath path;
	QVector<QPointF> polygonPoints;

	if(second == NULL)
		return path;

	float	x1 = first->pos().x(),
			y1 = first->pos().y(),
			x2 = second->pos().x(),
			y2 = second->pos().y();
	float	X = x2-x1,
			Y = y2-y1;

	if(abs(X) > abs(Y))
	{
		if(x1 <= x2 && y1 <= y2)
			{
				polygonPoints	<< QPointF(x1, y1 - 7)
								<< QPointF(x1 + X/2 + 7, y1 - 7)
								<< QPointF(x1 + X/2 + 7, y2 - 7)
								<< QPointF(x2, y2 - 7)
								<< QPointF(x2, y2 + 7)
								<< QPointF(x1 + X/2 -7, y2 + 7)
								<< QPointF(x1 + X/2 -7, y1 + 7)
								<< QPointF(x1, y1 + 7);
			}
		else if(x1 <= x2 && y1 >= y2)
			{
				polygonPoints	<< QPointF(x1, y1 - 7)
								<< QPointF(x1 + X/2 - 7, y1 - 7)
								<< QPointF(x1 + X/2 - 7, y2 - 7)
								<< QPointF(x2, y2 - 7)
								<< QPointF(x2, y2 + 7)
								<< QPointF(x1 + X/2 + 7, y2 + 7)
								<< QPointF(x1 + X/2 + 7, y1 + 7)
								<< QPointF(x1, y1 + 7);
			}
		else if(x1 >= x2 && y1 <= y2)
			{
				polygonPoints	<< QPointF(x1, y1 - 7)
								<< QPointF(x1 + X/2 - 7, y1 - 7)
								<< QPointF(x1 + X/2 - 7, y2 - 7)
								<< QPointF(x2, y2 - 7)
								<< QPointF(x2, y2 + 7)
								<< QPointF(x1 + X/2 + 7, y2 + 7)
								<< QPointF(x1 + X/2 + 7, y1 + 7)
								<< QPointF(x1, y1 + 7);
			}
		else if(x1 >= x2 && y1 >= y2)
			{
				polygonPoints	<< QPointF(x1, y1 - 7)
								<< QPointF(x1 + X/2 + 7, y1 - 7)
								<< QPointF(x1 + X/2 + 7, y2 - 7)
								<< QPointF(x2, y2 - 7)
								<< QPointF(x2, y2 + 7)
								<< QPointF(x1 + X/2 - 7, y2 + 7)
								<< QPointF(x1 + X/2 - 7, y1 + 7)
								<< QPointF(x1, y1 + 7);
			}
		}
		else
		{
			if(x1 <= x2 && y1 <= y2)
			{
				polygonPoints	<< QPointF(x1 + 7, y1)
								<< QPointF(x1 + 7, y1 +Y/2 - 7)
								<< QPointF(x2 + 7, y1 + Y/2 - 7)
								<< QPointF(x2 + 7, y2)
								<< QPointF(x2 - 7, y2)
								<< QPointF(x1 - 7, y1 + Y/2 +7)
								<< QPointF(x1 - 7, y1 + Y/2 +7)
								<< QPointF(x1 - 7, y1);
			}
			else if(x1 <= x2 && y1 >= y2)
			{
				polygonPoints	<< QPointF(x1 + 7, y1)
								<< QPointF(x1 + 7, y1 + Y/2 + 7)
								<< QPointF(x2 + 7, y1 + Y/2 + 7)
								<< QPointF(x2 + 7, y2)
								<< QPointF(x2 - 7, y2)
								<< QPointF(x2 - 7, y1 + Y/2 - 7)
								<< QPointF(x1 - 7, y1 + Y/2 - 7)
								<< QPointF(x1 - 7, y1);
			}
			else if(x1 >= x2 && y1 <= y2)
			{
				polygonPoints	<< QPointF(x1 + 7, y1)
								<< QPointF(x1 + 7, y1 +Y/2 + 7)
								<< QPointF(x2 + 7, y1 + Y/2 + 7)
								<< QPointF(x2 + 7, y2)
								<< QPointF(x2 - 7, y2)
								<< QPointF(x2 - 7, y1 + Y/2 - 7)
								<< QPointF(x1 - 7, y1 + Y/2 - 7)
								<< QPointF(x1 - 7, y1);
			}
			else if(x1 >= x2 && y1 >= y2)
			{
				polygonPoints	<< QPointF(x1 + 7, y1)
								<< QPointF(x1 + 7, y1 +Y/2 - 7)
								<< QPointF(x2 + 7, y1 + Y/2 - 7)
								<< QPointF(x2 + 7, y2)
								<< QPointF(x2 - 7, y2)
								<< QPointF(x2 - 7, y1 + Y/2 + 7)
								<< QPointF(x1 - 7, y1 + Y/2 + 7)
								<< QPointF(x1 - 7, y1);
			}
		}

	path.addPolygon(QPolygonF(polygonPoints));
	return path;
}

void Arrow::contextMenuEvent(QGraphicsSceneContextMenuEvent*mouseEvent)
{
	if(this->type() == arrowType::line || this->type() == arrowType::point || this->type() == arrowType::doubleLine)
	{
		this->setSelected(true);
		this->contextMenu->exec(mouseEvent->screenPos());
	}
	else
		return;
}