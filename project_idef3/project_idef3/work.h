#ifndef WORK_H
#define WORK_H

#include <QGraphicsLineItem>
#include <QGraphicsTextItem>
#include <QMenu>
#include <QtGui/QTextBlock>
#include <QWhatsThis>

#include "block.h"

class Work : public Block
{
public:
	Work(QGraphicsItem *parent, QMenu*);
	~Work();

	// ��������� ������
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	// �������� ������, ������� �������
	QRectF boundingRect() const;
	
	// ��������� ������ ������
	void setNumber(QString num) {
		Number = num;
	}

	// �������� ����� ������
	QString getNumber() {
		return Number;
	}
	
	// ������� ������ ������
	void setText(QString txt) {
		TextWork = txt;
	}
	
	// �������� ����� ������
	QString getText(){
		return TextWork;
	}

private:	
	QString Number;
	QString TextWork;
	QGraphicsTextItem *Text;
	qreal width, height;
	QMenu* contextMenu;
};

#endif // WORK_H
