#include "cross.h"

//�����������
Cross::Cross(QGraphicsItem *parent)
	: Block(parent, NULL)
{
	//��������� ������ ����������� � ���������
	this->setFlag(QGraphicsItem::ItemIsMovable);
	this->setFlag(QGraphicsItem::ItemIsSelectable);

	//��������� ���� ��������, ������� - �����������
	Block::setElementType(ElementType::cross);
	//������� ����� � ������������ �������� ������������
	xorImage = new QImage(qApp->applicationDirPath() + "/crossImages/xor.png");
	sorImage = new QImage(qApp->applicationDirPath() + "/crossImages/sor.png");
	aorImage = new QImage(qApp->applicationDirPath() + "/crossImages/aor.png");
	sandImage = new QImage(qApp->applicationDirPath() + "/crossImages/sand.png");
	aandImage = new QImage(qApp->applicationDirPath() + "/crossImages/aand.png");
}

Cross::~Cross()
{

}

//���������
void Cross::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0)
{
	//��������� ������ ��������� - ��������� �� ������������
	painter->setRenderHint(QPainter::Antialiasing, true);
	//������� - �����
	painter->setBrush(Qt::white);
	
	//���� ����������� �������
	if(this->isSelected())
	{
		//������ ��� ������� ��������
		painter->setPen(QPen(Qt::red));
		//���������� �� ������ ����
		this->setZValue(500);
	}
	else
	{
		//����� ��������� ��� ��� ����
		this->setZValue(0);
		painter->setPen(QPen(Qt::black));
	}

	//�������� �������, ���������� ������������
	QRectF rect = this->boundingRect();

	//�������� ���������� �������� ������ ����, � ����� ������ � ������
	int x = rect.x(), y = rect.y(), h = y+rect.height(), w = x+rect.width();

	//������ �����������
	//�������������
	painter->drawRect(rect);
	//�����, ������� ���� � ���� ����� ������������
	painter->drawLine(x+5, y, x+5, y+50);
	//� ����������� �� ���� ������������ ����������� � ������ ������������
	if(this->cType == crossType::SinchroAnd)
	{
		painter->drawImage(x+6, y+1, *sandImage);
		painter->drawLine(x+45, y, x+45, y+50);
	}
	if(this->cType == crossType::SinchroOr)
	{
		painter->drawImage(x+6, y+1, *sorImage);
		painter->drawLine(x+45, y, x+45, y+50);
	}
	if(this->cType == crossType::AsinchroAnd)
	{
		painter->drawImage(x+6, y+1, *aandImage);
	}
	if(this->cType == crossType::AsinchroOr)
	{
		painter->drawImage(x+6, y+1, *aorImage);
	}
	if(this->cType == crossType::Xor)
	{
		painter->drawImage(x+6, y+1, *xorImage);
	}
}

//�������� �������, ���������� ������������
QRectF Cross::boundingRect() const 
{
	return QRectF(-25, -25, 50, 50);
}