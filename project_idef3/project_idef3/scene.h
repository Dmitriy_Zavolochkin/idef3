#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <QKeyEvent>

#include "work.h"
#include "cross.h"
#include "arrow.h"

enum ActionType{ addWork, addCross1, addCross2, addCross3, addCross4, addCross5, actCursor, addArrow1, addArrow2, addArrow3 };
//enum TextType{ elementWork, elementArrow };

class Scene : public QGraphicsScene
{
	Q_OBJECT

public:
	Scene(QObject *parent, QMenu*, QMenu*);
	~Scene();

	QMenu *contextWorkMenu, *contextArrowMenu;
	
	void setActionType(ActionType type) {
		this->type = type;
	}

	/*Arrow* getArrow() {
		return this->arrow;
	}*/

	void startEditingElement();
	QVector<QString> getTextsElement();
	void addTextElement(QVector<QString>);
	//void setTypeText(TextType typetxt){this->eType = typetxt;}
	/*Arrow* getArrowToPosition(QGraphicsSceneMouseEvent *mouseEvent)
	{
		QGraphicsItem *item = this->itemAt(mouseEvent->scenePos());
		Arrow *elem = qgraphicsitem_cast<Arrow*>(item);
		return elem;
	}*/
	

protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
	/*void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);*/
	//void clearScene(QGraphicsScene scene);
	void keyPressEvent(QKeyEvent *keyEvent);

private:
//	TextType eType;
	ActionType type;
	
	QPointF startCopy;
	QPointF endCopy;
	
	Arrow *arrow;
	QGraphicsItem *editableElement;
	QList<Work*> copyWorks;
	QList<Cross*> copyCross;
	QList<Arrow*> copyArrows;
};

#endif // SCENE_H
