#ifndef TEXTBLOCK_H
#define TEXTBLOCK_H

#include <QDialog>
#include "ui_textblock.h"

class TextBlock : public QDialog
{
	Q_OBJECT

public:
	TextBlock(QWidget *parent = 0);
	~TextBlock();
	void Show(QString,QString);

private:
	Ui::TextBlock ui;

	private slots:
		void saveText();
		void cancel();

signals:
		void oktextblock(QString,QString);
};

#endif // TEXTBLOCK_H
