#include "textarrow.h"

TextArrow::TextArrow(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	this->setModal(true);
	connect(ui.OK, SIGNAL(clicked()), this, SLOT(saveText()));
	connect(ui.Cancel, SIGNAL(clicked()), this, SLOT(cancel()));
}

TextArrow::~TextArrow()
{

}

void TextArrow::Show(QString text)
{
	ui.comment->setText(text);
	this->show();
}

void TextArrow::saveText()
{
	emit oktextarrow(ui.comment->text());
	this->hide();
}

void TextArrow::cancel()
{
	this->hide();
}