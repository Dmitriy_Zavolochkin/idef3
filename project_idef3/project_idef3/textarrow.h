#ifndef TEXTARROW_H
#define TEXTARROW_H

#include <QDialog>
#include "ui_textarrow.h"

class TextArrow : public QDialog
{
	Q_OBJECT

public:
	TextArrow(QWidget *parent = 0);
	~TextArrow();
	void Show(QString);

private:
	Ui::TextArrow ui;

	private slots:
		void saveText();
		void cancel();

signals:
		void oktextarrow(QString);
};

#endif // TEXTARROW_H
