#include "scene.h"

Scene::Scene(QObject *parent, QMenu *workMenu, QMenu *arrowMenu)
	: QGraphicsScene(parent)
{
	this->contextWorkMenu = workMenu;
	this->contextArrowMenu = arrowMenu;
	arrow = NULL;
}

Scene::~Scene()
{

}

void Scene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	startCopy = mouseEvent->scenePos();
	if ( QApplication::keyboardModifiers() == Qt::ControlModifier )
	{
		foreach ( QGraphicsItem* item, this->selectedItems() )
		{
			if( item->type() == ElementType::work )
			{

				Work *elem = qgraphicsitem_cast<Work*>(item);
				copyWorks.append(elem);
				Work *work = new Work(NULL,this->contextWorkMenu);
				work->setNumber(elem->getNumber());
				work->setText(elem->getText());
				this->addItem(work);
				work->setPos(elem->x()-10, elem->y());
				elem->setSelected(false);
				work->setZValue(500);
				work->setSelected(true);
			}
			else if ( item->type() == crossType::AsinchroAnd || item->type() == crossType::AsinchroOr ||	item->type() == crossType::SinchroAnd
				|| item->type() == crossType::SinchroOr || item->type() == crossType::Xor )
			{
				Cross *elem = qgraphicsitem_cast<Cross*>(item);
				copyCross.append(elem);
				Cross *crosss = new Cross(NULL);
				crosss->setCrossType(elem->getType());
				this->addItem(crosss);
				crosss->setPos(elem->x()-10, elem->y());
				elem->setSelected(false);
				crosss->setZValue(500);
				crosss->setSelected(true);
			}
		}
		foreach ( QGraphicsItem* item, this->selectedItems() )
		{
			if( item->type() == arrowType::doubleLine || item->type() == arrowType::line || item->type() == arrowType::point )
			{
				Arrow *elem = dynamic_cast<Arrow*>(item);
				copyArrows.append(elem);

				Block *f_i = qgraphicsitem_cast<Block*>(this->itemAt(elem->getX1()-10, elem->getY1()));
				Block *s_i = qgraphicsitem_cast<Block*>(this->itemAt(elem->getX2()-10, elem->getY2()));

				if ( f_i->isSelected() && s_i->isSelected() )
				{
					Arrow *line = new Arrow(NULL, this->contextArrowMenu);
					line->setArrowType(elem->getType());

					line->setFirstItem(f_i);
					line->setSecondItem(s_i);

					f_i->addArrow(line);
					s_i->addArrow(line);

					line->setTextArrow(elem->getTextArrow());

					this->addItem(line);
					line->update();

					elem->setSelected(false);
					elem->setZValue(-1);
					line->setSelected(true);
				}
				else
				{
					elem->setSelected(false);
				}
			} 
		}
	}
	//else
	//{
		switch(type)
		{
		case ActionType::addWork:
			{
				Work *w;
				w = new Work(NULL, this->contextWorkMenu);
				this->addItem(w);
				w->setPos(mouseEvent->scenePos());
				this->type = ActionType::actCursor;
				break;
			}
		case ActionType::addCross1:
			{
				Cross *c;
				c = new Cross(NULL);
				c->setCrossType(crossType::Xor);
				this->addItem(c);
				c->setPos(mouseEvent->scenePos());
				this->type = ActionType::actCursor;
				break;
			}
		case ActionType::addCross2:
			{
				Cross *c;
				c = new Cross(NULL);
				c->setCrossType(crossType::SinchroOr);
				this->addItem(c);
				c->setPos(mouseEvent->scenePos());
				this->type = ActionType::actCursor;
				break;
			}
		case ActionType::addCross3:
			{
				Cross *c;
				c = new Cross(NULL);
				c->setCrossType(crossType::SinchroAnd);
				this->addItem(c);
				c->setPos(mouseEvent->scenePos());
				this->type = ActionType::actCursor;
				break;
			}
		case ActionType::addCross4:
			{
				Cross *c;
				c = new Cross(NULL);
				c->setCrossType(crossType::AsinchroOr);
				this->addItem(c);
				c->setPos(mouseEvent->scenePos());
				this->type = ActionType::actCursor;
				break;
			}
		case ActionType::addCross5:
			{
				Cross *c;
				c = new Cross(NULL);
				c->setCrossType(crossType::AsinchroAnd);
				this->addItem(c);
				c->setPos(mouseEvent->scenePos());
				this->type = ActionType::actCursor;
				break;
			}
		case ActionType::addArrow1:
			{
				if(arrow == NULL)
				{
					QGraphicsItem *item = this->itemAt(mouseEvent->scenePos());
					Block *elem = qgraphicsitem_cast<Block*>(item);

					if(elem != NULL)
					{
						arrow = new Arrow(NULL, elem, this->contextArrowMenu);
						arrow->setArrowType(arrowType::line);
						this->addItem(arrow);
						elem->addArrow(arrow);
					}
				}
				else
				{
					QGraphicsItem *item = this->itemAt(mouseEvent->scenePos());
					Block *elem = qgraphicsitem_cast<Block*>(item);

					if(elem != NULL)
					{
						arrow->setSecondItem(elem);
						arrow->update();
						elem->addArrow(arrow);
						arrow = NULL;
						type = ActionType::actCursor;
					}
				}
				break;
			}
		case ActionType::addArrow2:
			{
				if(arrow == NULL)
				{
					QGraphicsItem *item = this->itemAt(mouseEvent->scenePos());
					Block *elem = qgraphicsitem_cast<Block*>(item);

					if(elem != NULL)
					{
						arrow = new Arrow(NULL, elem, this->contextArrowMenu);
						arrow->setArrowType(arrowType::point);
						this->addItem(arrow);
						elem->addArrow(arrow);
					}
				}
				else
				{
					QGraphicsItem *item = this->itemAt(mouseEvent->scenePos());
					Block *elem = qgraphicsitem_cast<Block*>(item);

					if(elem != NULL)
					{
						arrow->setSecondItem(elem);
						arrow->update();
						elem->addArrow(arrow);
						arrow = NULL;
						type = ActionType::actCursor;
					}
				}
				break;
			}
		case ActionType::addArrow3:
			{
				if(arrow == NULL)
				{
					QGraphicsItem *item = this->itemAt(mouseEvent->scenePos());
					Block *elem = qgraphicsitem_cast<Block*>(item);

					if(elem != NULL)
					{
						arrow = new Arrow(NULL, elem, this->contextArrowMenu);
						arrow->setArrowType(arrowType::doubleLine);
						this->addItem(arrow);
						elem->addArrow(arrow);
					}
				}
				else
				{
					QGraphicsItem *item = this->itemAt(mouseEvent->scenePos());
					Block *elem = qgraphicsitem_cast<Block*>(item);

					if(elem != NULL)
					{
						arrow->setSecondItem(elem);
						arrow->update();
						elem->addArrow(arrow);
						arrow = NULL;
						type = ActionType::actCursor;
					}
				}
				break;
			}
		case ActionType::actCursor:
			{
				QGraphicsScene::mousePressEvent(mouseEvent);
				break;
			}
		}
	//}
}

//void Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
//{
//	if ( QApplication::keyboardModifiers() == Qt::AltModifier )
//	{
//
//		foreach ( QGraphicsItem* item, this->selectedItems() )
//		{
//			if( item->type() == ElementType::work )
//			{
//				Work *elem = qgraphicsitem_cast<Work*>(item);
//				copyWorks.append(elem);
//			}
//			else if( item->type() == arrowType::doubleLine || item->type() == arrowType::line || item->type() == arrowType::point )
//			{
//				Arrow *elem = dynamic_cast<Arrow*>(item);
//				copyArrows.append(elem);
//			} 
//			else if ( item->type() == crossType::AsinchroAnd || item->type() == crossType::AsinchroOr ||	item->type() == crossType::SinchroAnd
//				|| item->type() == crossType::SinchroOr || item->type() == crossType::Xor )
//			{
//				Cross *elem = qgraphicsitem_cast<Cross*>(item);
//				copyCross.append(elem);
//			}
//		}
//
//		qreal dx, dy;
//		if ( !(copyWorks.isEmpty() && copyCross.isEmpty()) )
//		{
//			endCopy = mouseEvent->scenePos();
//			dx = endCopy.x() - startCopy.x();
//			dy = endCopy.y() - startCopy.y();
//			foreach(Work* item, copyWorks)
//			{
//				this->addItem(item);
//				item->setPos(item->x()+dx, item->y()+dy);
//			}
//			foreach(Cross* item, copyCross)
//			{
//				this->addItem(item);
//				item->setPos(item->x()+dx, item->y()+dy);
//			}
//			foreach(Arrow* item, copyArrows)
//			{
//				Block *f = qgraphicsitem_cast<Block*>(item->firstItem());
//				Block *s = qgraphicsitem_cast<Block*>(item->secondItem());
//
//				item->setFirstItem(f);
//				item->setSecondItem(s);
//
//				f->addArrow(item);
//				s->addArrow(item);
//
//				this->addItem(item);
//				item->update();
//			}
//		}
//	}
//}
//
//void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
//{
//	if ( QApplication::keyboardModifiers() == Qt::AltModifier )
//	{
//		copyWorks.clear();
//		copyCross.clear();
//		copyArrows.clear();
//	}
//}

void Scene::startEditingElement()
{
	QList<QGraphicsItem*>items = this->selectedItems();
	if(items.size() != 1)
		return;
	this->editableElement = items[0];
}

QVector<QString> Scene::getTextsElement()
{
	QVector<QString> texts;
	int typetxt = this->editableElement->type();
	switch(typetxt)
	{
	case ElementType::work:
		{
			texts << QString("Work");
			Work *elem = qgraphicsitem_cast<Work*>(this->editableElement);
			texts << elem->getText() << elem->getNumber();
			break;
		}
	case arrowType::line:
		{
			texts << QString("Line");
			Arrow *elem = dynamic_cast<Arrow*>(this->editableElement);
			elem->setArrowType(arrowType::line);
			texts << elem->getTextArrow();
			break;
		}
	case arrowType::point:
		{
			texts << QString("Point");
			Arrow *elem = dynamic_cast<Arrow*>(this->editableElement);
			elem->setArrowType(arrowType::point);
			texts << elem->getTextArrow();
			break;
		}
	case arrowType::doubleLine:
		{
			texts << QString("Double");
			Arrow *elem = dynamic_cast<Arrow*>(this->editableElement);
			elem->setArrowType(arrowType::doubleLine);
			texts << elem->getTextArrow();
			break;
		}
	default:
		{
			break;
		}
	}
	return texts;
}

void Scene::addTextElement(QVector<QString>elements)
{
	if(this->editableElement == NULL)
		return;
	int typetxt = this->editableElement->type();

	switch(typetxt)
	{
	case ElementType::work:
		{
			Work *elem = qgraphicsitem_cast<Work*>(this->editableElement);
			elem->setText(elements[0]);
			elem->setNumber(elements[1]);
			break;
		}
	case arrowType::line:
		{
			Arrow *elem = dynamic_cast<Arrow*>(this->editableElement);
			elem->setTextArrow(elements[0]);
			break;
		}
	case arrowType::point:
		{
			Arrow *elem = dynamic_cast<Arrow*>(this->editableElement);
			elem->setTextArrow(elements[0]);
			break;
		}
	case arrowType::doubleLine:
		{
			Arrow *elem = dynamic_cast<Arrow*>(this->editableElement);
			elem->setTextArrow(elements[0]);
			break;
		}
	default:
		{
			break;
		}
	}
}

//void Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
//{
//	if(type == ActionType::addArrow1 && arrow != NULL)
//	{
//		arrow->updateToPoint(mouseEvent->scenePos());
//		return;
//	}
//
//	QGraphicsScene::mouseMoveEvent(mouseEvent);
//}

void Scene::keyPressEvent(QKeyEvent *keyEvent)
{
	switch(keyEvent->key())
	{
	case Qt::Key_Delete:
		{
			if(!selectedItems().size())
				return;

			foreach (QGraphicsItem* arrow, selectedItems())
			{
				Arrow *item = dynamic_cast<Arrow*>(arrow);
				if(item == NULL)
					continue;
				if(item->type() == arrowType::line || item->type() == arrowType::point || item->type() == arrowType::doubleLine)
				{
					Arrow *arrowItem = dynamic_cast<Arrow*>(item);
					this->removeItem(item);
					Block *elem = qgraphicsitem_cast<Block*>(arrowItem->firstItem());
					elem->removeArrow(arrowItem);
					elem = qgraphicsitem_cast<Block*>(arrowItem->secondItem());
					elem->removeArrow(arrowItem);
					delete arrowItem;
				}
			}
			foreach(QGraphicsItem* item, this->selectedItems())
			{
				if(item->type() == ElementType::work || item->type() == crossType::AsinchroAnd || item->type() == crossType::AsinchroOr 
					|| item->type() == crossType::SinchroAnd || item->type() == crossType::SinchroOr || item->type() == crossType::Xor)
				{
					Block *blockItem = qgraphicsitem_cast<Block*>(item);
					blockItem->removeArrows();
				}
				this->removeItem(item);
				delete item;
			}
			break;
		}
	}
	/*if ( !( QApplication::keyboardModifiers() == Qt::AltModifier ) )
	{
		this->type = ActionType::actCursor;
	}*/
}