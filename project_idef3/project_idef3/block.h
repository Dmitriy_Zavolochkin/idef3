#ifndef BLOCK_H
#define BLOCK_H

#include <QGraphicsItem>
#include <QRectF>
#include <QPainter>
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>

#include "arrow.h"

enum ElementType { work = 65541, cross = 65542 };
//enum TextType { textWork = 65539 };

class Block : public QGraphicsItem
{
public:
	Block(QGraphicsItem*, QMenu*);
	~Block();

	// �������� ��� ��������
	enum ElementType getElementType() {
		return this->eType;
	}

	// ������������� ��� ��������
	void setElementType(ElementType type) {
		this->eType = type;
	}
	
	int type()const {
		return this->eType;
	}

	void setTextType(ElementType typeText) {
		this->eType = typeText;
	}

	// ������� ��� �������� ������������ ����
	void contextMenuEvent(QGraphicsSceneContextMenuEvent*);

	// ���������, ���������� �� �������
	bool addArrow(Arrow*);

	// ��������� ��������� ��������
	QVariant itemChange(GraphicsItemChange, const QVariant&);

	// �������� �������
	void removeArrow(Arrow*);

	// �������� �������
	void removeArrows();

private:
	enum ElementType eType;
	enum TextType eTextType;
	QMenu *contextMenu;
	QVector<Arrow*> arrows;
	qreal width,height;
};

#endif // BLOCK_H