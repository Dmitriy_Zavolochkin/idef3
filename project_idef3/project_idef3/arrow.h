#ifndef ARROW_H
#define ARROW_H

#include <QApplication>
#include <QGraphicsLineItem>
#include <QGraphicsItem>
#include <QPainter>
#include <QImage>
#include <QGraphicsSceneContextMenuEvent>
#include <QMenu>
#include <QMessageBox>

enum arrowType { line = 65543, point = 65544, doubleLine = 65545 };

class Arrow : public QGraphicsLineItem
{
public:
	Arrow(QGraphicsItem *parent, QMenu *menu);
	Arrow(QGraphicsItem *parent, QGraphicsItem *b, QMenu*);
	~Arrow();

	void paint(QPainter *painter,const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

	QGraphicsItem* firstItem() {
		return first;
	}

	QGraphicsItem* secondItem() {
		return second;
	}

	//int typeTXT()const{return this->eType;}
	
	void setFirstItem(QGraphicsItem*elem) {
		first = elem;
	}

	void setSecondItem(QGraphicsItem*elem) {
		second = elem;
	}

	void update();
	void updateToPoint(QPointF);

	void setSecondPoint(QPointF point) {
		secondPoint = point;
	}

	void setArrowType(arrowType type) {
		this->aType = type;
	}

	int type() const {
		return this->aType;
	}

	arrowType getType() {
		return this->aType;
	}

	int getX1() {
		int f_x = this->first->scenePos().x();
		return f_x/*this->first->scenePos().x()*/;
	}

	int getY1() {
		return this->first->scenePos().y();
	}

	int getX2() {
		return this->second->scenePos().x();
	}

	int getY2() {
		return this->second->scenePos().y();
	}

	QString getTextArrow() {
			return textArrow;
	}

	void setTextArrow(QString txt) {
		textArrow = txt;
	}

	QRectF boundingRect() const;
	QPainterPath shape() const;

	void contextMenuEvent(QGraphicsSceneContextMenuEvent*mouseEvent);

private:
	QString textArrow;
	QGraphicsItem *first;
	QGraphicsItem *second;
	QPointF secondPoint;
	arrowType aType;
	QMenu *contextMenu;
	QImage *arrowHeadRight, *arrowHeadLeft, *arrowHeadUp, *arrowHeadDown;
};

#endif // ARROW_H
