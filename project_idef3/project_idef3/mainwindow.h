#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QGraphicsView>
#include <QString>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QApplication>
#include <QSettings>
#include <QIcon>

#include "ui_mainwindow.h"
#include "scene.h"
#include "textblock.h"
#include "textarrow.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0, Qt::WFlags flags = 0);
	~MainWindow();

private:
	Ui::MainWindowClass ui;

	void createWorkContextMenu();
	void createArrowContextMenu();

	QGraphicsView *view;
	Scene *scene;
	QMenu *WorkContextMenu, *ArrowContextMenu;
	TextBlock *textWork;
	TextArrow *textArrow;
	
	private slots:
		void addWork();
		void addCross1();
		void addCross2();
		void addCross3();
		void addCross4();
		void addCross5();
		void actionNew();
		void actionSave();
		void actionSave_as();
		void actionOpen();
		void actionExport_to_PNG();
		void actionExport_to_PNG_with();
		void actionExit();
		void editWorkContextMenuClick();
		void editArrowContextMenuClick();
		void addTextWork(QString,QString);
		void addTextArrow(QString);
		void addArrow1();
		void addArrow2();
		void addArrow3();
		void scaleUp();
		void scaleDown();
		void closeEvent(QCloseEvent * event);

public:
	void writeSettings();
	void readSettings();
};

#endif // MAINWINDOW_H
